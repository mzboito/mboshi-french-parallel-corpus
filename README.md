# Mboshi-French parallel corpus
This is a fork from the original repository, available at github.com/besacier/mboshi-french-parallel-corpus

## The content of this repository is the following:
* Forced alignment, wav, translation and transcription files (same from the original repository)
* Pseudo phones (with and without gold silence information) extracted using zrc recipe from beer-asr/asr
* True phones individual files (extracted form the limsi-align)

=> New ZRC reference is available [here](https://github.com/mzboito/ZRC_corpora).

## Citing:
Please use the following bibtex reference to cite our corpus and LREC2018 paper, available at http://arxiv.org/abs/1710.03501

@article{godard2017very,
  title={A very low resource language speech corpus for computational language documentation experiments},
  author={Godard, Pierre and Adda, Gilles and Adda-Decker, Martine and Benjumea, Juan and Besacier, Laurent and Cooper-Leavitt, Jamison and Kouarata, Guy-No{\"e}l and Lamel, Lori and Maynard, H{\'e}l{\`e}ne and M{\"u}ller, Markus and others},
  journal={arXiv preprint arXiv:1710.03501},
  year={2017}
}
 
